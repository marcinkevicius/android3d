#include <jni.h>
#include <string>
#include <vector>
#include <android/log.h>

extern "C" {

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/avfilter.h>

AVCodec* codec;
AVCodecContext* codec_context;
AVCodecParserContext* parser;
int frame_count=0;

jobject g_obj;
jmethodID g_mid;

JNIEXPORT jint JNICALL
Java_com_example_kid_a3dstream_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject jobject1/* this */) {
    int status_code=0;

    g_obj = env->NewGlobalRef(jobject1);
    // save refs for callback
    jclass g_clazz = env->GetObjectClass(g_obj);
    if (g_clazz == NULL) { //Failed to find class
        status_code=-4;
    }

    g_mid = env->GetMethodID(g_clazz, "gotFrameCallback", "([BIIZII)V");
    if (g_mid == NULL) { //Unable to get method ref
        status_code=-5;
    }

    avcodec_register_all();
    av_register_all();
    // prepare values for parsing bitstream

    parser = av_parser_init(AV_CODEC_ID_H264);
    if(!parser) {
        status_code = -1; // problems setting up variables
    }
    codec = avcodec_find_decoder(AV_CODEC_ID_H264);
    if(!codec) {
        status_code = -2; // problems setting up variables
    }
    codec_context = avcodec_alloc_context3(codec);
    if(codec->capabilities & CODEC_CAP_TRUNCATED) {
        codec_context->flags |= CODEC_FLAG_TRUNCATED;
    }

//    codec_context->thread_count = 4; // ?????
//    codec_context->thread_type = FF_THREAD_FRAME; // ??????

    if(avcodec_open2(codec_context, codec, NULL) < 0) {
        status_code = -3; // problems setting up variables
    }

    return status_code;
}

void invokeFrameDataCallback(JNIEnv *env, jobject obj, uint8_t* framebuf, int size, int frameNum, int isKeyFrame, int width, int height)
{
    jbyte* buff = (jbyte*)framebuf;
    jbyteArray jarray = env->NewByteArray(size);
    env->SetByteArrayRegion(jarray, 0, size, buff);
    env->CallVoidMethod(g_obj, g_mid, jarray, size, frameNum, isKeyFrame!=0, width, height);
}

jint Java_com_example_kid_a3dstream_TcpClient_parseStream(
        JNIEnv *env, jobject jobject1, jint offset, jbyteArray streambuffer){

//    AVPacket packet;
//    av_init_packet(&packet);

    int bufflen = env->GetArrayLength (streambuffer);

    jbyte* jBuff = (jbyte*)(env->GetByteArrayElements(streambuffer, NULL));
    uint8_t* buf = (uint8_t*) jBuff;

    uint8_t* data = NULL;
    int size = 0;

//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "start int8_t = %u", buf[0]);
    int len = av_parser_parse2(parser, codec_context, &data, &size, buf, bufflen, 0, 0, AV_NOPTS_VALUE);

//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "parsed bytes inside jdk = %d, buffer size %d size %d", len, bufflen, size);

    if(size == 0 && len >= 0) {

        env->ReleaseByteArrayElements(streambuffer, jBuff, 0);
//        av_packet_unref(&packet);
        return 0;
    } else {
        env->ReleaseByteArrayElements(streambuffer, jBuff, 0);
//        av_packet_unref(&packet);
        return len;
    }
}

jint Java_com_example_kid_a3dstream_MainActivity_parseStream(
        JNIEnv *env, jobject jobject1, jint offset, jbyteArray streambuffer){

    AVPacket packet;
    av_init_packet(&packet);

    int bufflen = env->GetArrayLength (streambuffer);

    jbyte* jBuff = (jbyte*)(env->GetByteArrayElements(streambuffer, NULL));
    uint8_t* buf = (uint8_t*) jBuff;

//    int8_t *buf = (int8_t*) env->GetByteArrayElements(streambuffer, NULL);
//    uint8_t buf = (uint8_t) env->GetByteArrayElements(streambuffer, NULL);
//    env->ReleaseByteArrayElements(streambuffer, bufferPtr, JNI_ABORT);

//    unsigned char* buf = new unsigned char[bufflen];
//    env->GetByteArrayRegion (streambuffer, 0, bufflen, reinterpret_cast<jbyte*>(buf));
//
    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "start int8_t = %u", buf[0]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[1]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[2]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[3]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[4]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[5]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[6]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[7]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[8]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[9]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[10]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[11]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[12]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "int8_t = %u", buf[13]);
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "uint8_t = %u", ((unsigned char) buf[8]));
//    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "streambuffer = %c", streambuffer[4]);
    int len = av_parser_parse2(parser, codec_context, &packet.data, &packet.size, buf, bufflen, 0, 0, AV_NOPTS_VALUE);
//
    __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "parsed bytes inside jdk = %d, buffer size %d pckt size %d", len, bufflen, packet.size);

//
//    if (packet.size > 0){ //1296 h 972
//        frame_count++;
//        __android_log_print(ANDROID_LOG_DEBUG, "HelloNDK!", "GOT FRAME!!! = %d w %d h %d s:%d", frame_count,parser->width, parser->height, packet.size);
//        invokeFrameDataCallback(env, jobject1, packet.data, packet.size, frame_count, parser->key_frame, parser->width, parser->height);
//
//    }

    if(packet.size == 0 && len >= 0) {

    env->ReleaseByteArrayElements(streambuffer, jBuff, 0);
    av_packet_unref(&packet);
    return 0;
    } else {
        env->ReleaseByteArrayElements(streambuffer, jBuff, 0);
        av_packet_unref(&packet);
        return len;
    }
}


jstring Java_com_example_kid_a3dstream_MainActivity_urlprotocolinfo(
        JNIEnv *env, jobject) {
    char info[40000] = {0};
    av_register_all();

    struct URLProtocol *pup = NULL;

    struct URLProtocol **p_temp = &pup;
    avio_enum_protocols((void **) p_temp, 0);

    while ((*p_temp) != NULL) {
        sprintf(info, "%sInput: %s\n", info, avio_enum_protocols((void **) p_temp, 0));
    }
    pup = NULL;
    avio_enum_protocols((void **) p_temp, 1);
    while ((*p_temp) != NULL) {
        sprintf(info, "%sInput: %s\n", info, avio_enum_protocols((void **) p_temp, 1));
    }
    return env->NewStringUTF(info);
}

jstring Java_com_example_kid_a3dstream_MainActivity_avformatinfo(
        JNIEnv *env, jobject) {
    char info[40000] = {0};

    av_register_all();

    AVInputFormat *if_temp = av_iformat_next(NULL);
    AVOutputFormat *of_temp = av_oformat_next(NULL);
    while (if_temp != NULL) {
        sprintf(info, "%sInput: %s\n", info, if_temp->name);
        if_temp = if_temp->next;
    }
    while (of_temp != NULL) {
        sprintf(info, "%sOutput: %s\n", info, of_temp->name);
        of_temp = of_temp->next;
    }
    return env->NewStringUTF(info);
}

jstring Java_com_example_kid_a3dstream_MainActivity_avcodecinfo(
        JNIEnv *env, jobject) {
    char info[40000] = {0};

    av_register_all();

    AVCodec *c_temp = av_codec_next(NULL);

    while (c_temp != NULL) {
        if (c_temp->decode != NULL) {
            sprintf(info, "%sdecode:", info);
        } else {
            sprintf(info, "%sencode:", info);
        }
        switch (c_temp->type) {
            case AVMEDIA_TYPE_VIDEO:
                sprintf(info, "%s(video):", info);
                break;
            case AVMEDIA_TYPE_AUDIO:
                sprintf(info, "%s(audio):", info);
                break;
            default:
                sprintf(info, "%s(other):", info);
                break;
        }
        sprintf(info, "%s[%10s]\n", info, c_temp->name);
        c_temp = c_temp->next;
    }

    return env->NewStringUTF(info);
}

jstring Java_com_example_kid_a3dstream_MainActivity_avfilterinfo(
        JNIEnv *env, jobject) {
    char info[40000] = {0};
    avfilter_register_all();

    AVFilter *f_temp = (AVFilter *) avfilter_next(NULL);
    while (f_temp != NULL) {
        sprintf(info, "%s%s\n", info, f_temp->name);
        f_temp = f_temp->next;
    }
    return env->NewStringUTF(info);
}
}