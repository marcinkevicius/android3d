package com.example.kid.a3dstream;

/**
 * Created by andrius on 28/03/2018.
 */

import android.opengl.GLES20;
import android.opengl.Matrix;
import android.util.Log;

import com.example.kid.a3dstream.shader.SimpleFragmentShader;
import com.example.kid.a3dstream.shader.SimpleShaderProgram;
import com.example.kid.a3dstream.shader.SimpleVertexShader;

import org.artoolkit.ar.base.ARToolKit;
import org.artoolkit.ar.base.rendering.gles20.ARRendererGLES20;
import org.artoolkit.ar.base.rendering.gles20.CubeGLES20;
import org.artoolkit.ar.base.rendering.gles20.ShaderProgram;

import java.util.Arrays;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import static java.lang.Math.PI;


public class SimpleGLES20Renderer extends ARRendererGLES20 {

    private int markerID = -1;
    private CubeGLES20 cube;
    private float [] modelViewMatrix = {
            1f, 0f, 0f, 0f,
            0f, 1f, 0f, 0f,
            0f, 0f, 1f, 0f,
            0f, 0f, -300f, 1f};

    private float [] null_modelViewMatrix = {
            1f, 0f, 0f, 0f,
            0f, 1f, 0f, 0f,
            0f, 0f, 1f, 0f,
            0f, 0f, -300f, 1f};

    private float[] projectionMatrix;
    private  int[] ypr_xyz;

    Long tsLastValid = System.currentTimeMillis();

    @Override
    public boolean configureARScene() {

        markerID = ARToolKit.getInstance().addMarker("single;Data/hiro.patt;80");
        if (markerID < 0) return false;

        return true;

    }

    public SimpleGLES20Renderer(int[]ypr_xyz){
        this.ypr_xyz=ypr_xyz;
    }

    //Shader calls should be within a GL thread that is onSurfaceChanged(), onSurfaceCreated() or onDrawFrame()
    //As the cube instantiates the shader during setShaderProgram call we need to create the cube here.
    @Override
    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        super.onSurfaceCreated(unused, config);

        ShaderProgram shaderProgram = new SimpleShaderProgram(new SimpleVertexShader(), new SimpleFragmentShader());
        cube = new CubeGLES20(20.0f, 0.0f, 0.0f, 0.0f);
        cube.setShaderProgram(shaderProgram);
    }

    @Override
    public void draw() {
        super.draw();

//        GLES20.glEnable(GLES20.GL_CULL_FACE);
        GLES20.glEnable(GLES20.GL_DEPTH_TEST);
        GLES20.glFrontFace(GLES20.GL_CW);

//        float [] blankModelViewMatrix = {1,0,0,0,   0,1,0,0,   0,0,1,0,   0,0,-300,0};

        float[] matTranslationY1 =
                {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        10,0,0,1};
        float[] matTranslationY2 =
                {1,0,0,0,
                        0,1,0,0,
                        0,0,1,0,
                        -20,0,0,1};

        float [] tmp_modelViewMatrix;

//        float[] projectionMatrix = ARToolKit.getInstance().getProjectionMatrix();
        //float[] modelViewMatrix = blankModelViewMatrix;

        if (ARToolKit.getInstance().queryMarkerVisible(markerID)) {
            cube.green=true;
            if (projectionMatrix == null)
                projectionMatrix = ARToolKit.getInstance().getProjectionMatrix();

            tsLastValid = System.currentTimeMillis();
            tmp_modelViewMatrix = ARToolKit.getInstance().queryMarkerTransformation(markerID);

            for (int x = 0; x < 16; x++) {
                    modelViewMatrix[x] = (modelViewMatrix[x] + tmp_modelViewMatrix[x]) / 2.0f;
            }
        } else { // if no visible marker for more than 0.5s, move object to default position
            if (System.currentTimeMillis()-tsLastValid > 500){
                cube.green=false;
                for (int x = 0; x < 16; x++) {
                    if (!(x==9 || x==5)) // update to default except pitch
                        modelViewMatrix[x] = (modelViewMatrix[x] + null_modelViewMatrix[x]) / 2.0f;
                }
            }
        }

        if (projectionMatrix != null && modelViewMatrix != null) {

            tmp_modelViewMatrix=modelViewMatrix;

            Matrix.multiplyMM(tmp_modelViewMatrix, 0, matTranslationY1, 0, modelViewMatrix, 0);
            GLES20.glViewport(0, 0, viewport_w / 2, viewport_h);
            cube.draw(projectionMatrix, tmp_modelViewMatrix);

            Matrix.multiplyMM(tmp_modelViewMatrix, 0, matTranslationY2, 0, modelViewMatrix, 0);
            GLES20.glViewport(viewport_w / 2, 0, viewport_w / 2, viewport_h);
            cube.draw(projectionMatrix, tmp_modelViewMatrix);

            Matrix.multiplyMM(tmp_modelViewMatrix, 0, matTranslationY1, 0, modelViewMatrix, 0);

            //arGetTransMat queryMarkerTransformation
            //Log.d("marker transformations", Arrays.toString(modelViewMatrix));
            // blank matrix
            //      1   0   0.25 0
            //      0   1   0    0
            //  -0.25   0   1    -300
            //      0   0   0    0
            // so blankModelViewMatrix = 1,0,-0.25,0,   0,1,0,0,   0.25,0,1,0,   0,0,-300,0
//            Log.d("marker transformations", Arrays.toString(projectionMatrix));
            /** this conversion uses conventions as described on page:
             *   http://www.euclideanspace.com/maths/geometry/rotations/euler/index.htm
             *   Coordinate System: right hand
             *   Positive angle: right hand
             *   Order of euler angles: heading first, then attitude, then bank
             *   matrix row column ordering:
             *   [m00 m01 m02]
             *   [m10 m11 m12]
             *   [m20 m21 m22]
             *
             *   heading = Math.atan2(-m.m20,m.m00);
             *   bank = Math.atan2(-m.m12,m.m11);
             *   attitude = Math.asin(m.m10);
             *   */
//            float[][] transformedMatrix = new float[4][4];
//            transformedMatrix[0][0]=modelViewMatrix[0];
//            transformedMatrix[1][0]=modelViewMatrix[1];
//            transformedMatrix[2][0]=modelViewMatrix[2];
//            transformedMatrix[3][0]=modelViewMatrix[3];
//            transformedMatrix[0][1]=modelViewMatrix[4];
//            transformedMatrix[1][1]=modelViewMatrix[5];
//            transformedMatrix[2][1]=modelViewMatrix[6];
//            transformedMatrix[3][1]=modelViewMatrix[7];
//            transformedMatrix[0][2]=modelViewMatrix[8];
//            transformedMatrix[1][2]=modelViewMatrix[9];
//            transformedMatrix[2][2]=modelViewMatrix[10];
//            transformedMatrix[3][2]=modelViewMatrix[11];
//            transformedMatrix[0][3]=modelViewMatrix[12];
//            transformedMatrix[1][3]=modelViewMatrix[13];
//            transformedMatrix[2][3]=modelViewMatrix[14];
//            transformedMatrix[3][3]=modelViewMatrix[15];
//            Log.d("marker transformations", "heading:   "+((180/PI) * Math.atan2(-transformedMatrix[2][0],transformedMatrix[0][0])));
//            Log.d("marker transformations", "bank:      "+((180/PI) * Math.atan2(-transformedMatrix[1][2],transformedMatrix[1][1])));
//            Log.d("marker transformations", "attitude:  "+((180/PI) * Math.asin(transformedMatrix[1][0])));

//            Log.d("marker transformations", "heading:   "+((180/PI) * Math.atan2(-modelViewMatrix[2],modelViewMatrix[0])));
//            Log.d("marker transformations", "pitch:      "+((180/PI) * Math.atan2(-modelViewMatrix[9],modelViewMatrix[5])));
//            Log.d("marker transformations", "roll:  "+((180/PI) * Math.asin(modelViewMatrix[1])));
//            Log.d("marker transformations", "x:  "+modelViewMatrix[12]+" y:  "+modelViewMatrix[13]+" z:  "+modelViewMatrix[14]);

            ypr_xyz[0]=(int) ((180/PI) * Math.atan2(-modelViewMatrix[2],modelViewMatrix[0]));
            ypr_xyz[1]=(int) ((180/PI) * Math.atan2(-modelViewMatrix[9],modelViewMatrix[5]));
            ypr_xyz[2]=(int) ((180/PI) * Math.asin(modelViewMatrix[1]));
            ypr_xyz[3]=(int) modelViewMatrix[12];
            ypr_xyz[4]=(int) modelViewMatrix[13];
            ypr_xyz[5]=(int) modelViewMatrix[14];
        }

    }
}
