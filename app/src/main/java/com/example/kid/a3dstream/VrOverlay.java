package com.example.kid.a3dstream;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ConfigurationInfo;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.os.Looper;
import android.util.Log;
import android.view.TextureView;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.artoolkit.ar.base.ARToolKit;
import org.artoolkit.ar.base.camera.CameraEventListener;
import org.artoolkit.ar.base.camera.CaptureCameraPreview;
import org.artoolkit.ar.base.rendering.ARRenderer;
import org.artoolkit.ar.base.rendering.gles20.ARRendererGLES20;

import static android.content.ContentValues.TAG;

/**
 * Created by andrius on 26/03/2018.
 */

public class VrOverlay implements Runnable, CameraEventListener {

//    artoolkit variables
    protected int pattSize = 16;
    protected int pattCountMax = 25;
    private boolean firstUpdate = false;
    private CaptureCameraPreview preview= null;
    private Activity mActivity = null;
    private GLSurfaceView glView;
    protected ARRenderer renderer;
    protected FrameLayout mainLayout;
    public int[] ypr_xyz={0,0,0,0,0,0}; // ypr_xyz - int values of Yaw, Pitch, Roll , X, Y, Z


    private Context mContext;
//    private TextureView mTextureView;

    public VrOverlay(Context context, Activity activity, TextureView mTextureView) {
        this.mContext = context;
        this.mActivity=activity;
//        this.mTextureView = mTextureView;
    }

    @Override
    public void run() {

        Looper.prepare();

        if (ARToolKit.getInstance().initialiseNativeWithOptions(mContext.getCacheDir().getAbsolutePath(), pattSize, pattCountMax) == false) { // Use cache directory for Data files.

            new AlertDialog.Builder(mContext)
                    .setMessage("The native library is not loaded. The application cannot continue.")
                    .setTitle("Error")
                    .setCancelable(true)
                    .setNeutralButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton){ mActivity.finish(); }
                            })
                    .show();

            return;
        }
        mainLayout= mActivity.findViewById(R.id.mainLayout);
//        final int main_frame_layout_width=(mActivity.findViewById(R.id.mainLayout)).getWidth();

//        mainLayout = supplyFrameLayout();
        if (mainLayout == null) {
            Log.e(TAG, "Error: supplyFrameLayout did not return a layout.");
            return;
        }

        renderer = supplyRenderer();
        if (renderer == null) {
            Log.e(TAG, "Error: supplyRenderer did not return a renderer.");
            // No renderer supplied, use default, which does nothing
            renderer = new ARRenderer();
        }

        preview = new CaptureCameraPreview(mActivity, this);

        Log.i(TAG, "CaptureCameraPreview constructed");

        if (preview.gettingCameraAccessPermissionsFromUser())
            //No need to go further, must ask user to allow access to the camera first.
            return;

        // Create the GL view
        glView = new GLSurfaceView(mContext);

        // Check if the system supports OpenGL ES 2.0.
        final ActivityManager activityManager = (ActivityManager)mActivity.getSystemService(Context.ACTIVITY_SERVICE);
        final ConfigurationInfo configurationInfo = activityManager.getDeviceConfigurationInfo();
        final boolean supportsEs2 = configurationInfo.reqGlEsVersion >= 0x20000;

        if (supportsEs2) {
            Log.i(TAG, "onResume(): OpenGL ES 2.x is supported");

            if (renderer instanceof ARRendererGLES20) {
                // Request an OpenGL ES 2.0 compatible context.
                glView.setEGLContextClientVersion(2);
            } else {
                Log.w(TAG, "onResume(): OpenGL ES 2.x is supported but only a OpenGL 1.x renderer is available." +
                        " \n Use ARRendererGLES20 for ES 2.x support. \n Continuing with OpenGL 1.x.");
                glView.setEGLContextClientVersion(1);
            }
        } else {
            Log.i(TAG, "onResume(): Only OpenGL ES 1.x is supported");
            if (renderer instanceof ARRendererGLES20)
                throw new RuntimeException("Only OpenGL 1.x available but a OpenGL 2.x renderer was provided.");
            // This is where you could create an OpenGL ES 1.x compatible
            // renderer if you wanted to support both ES 1 and ES 2.
            glView.setEGLContextClientVersion(1);
        }

        glView.setEGLConfigChooser(8, 8, 8, 8, 16, 0);
        glView.getHolder().setFormat(PixelFormat.TRANSLUCENT); // Needs to be a translucent surface so the camera preview shows through.
        glView.setRenderer(renderer);
        glView.setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // Only render when we have a frame (must call requestRender()).
        glView.setZOrderMediaOverlay(true); // Request that GL view's SurfaceView be on top of other SurfaceViews (including CameraPreview's SurfaceView).

        Log.i(TAG, "GLSurfaceView created");

        // Add the views to the interface
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
//                mainLayout.addView(preview, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT));
                mainLayout.addView(preview, new ViewGroup.LayoutParams(320,240));
//                mainLayout.addView(preview, new ViewGroup.LayoutParams(214,160));
                mainLayout.addView(glView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            }
        });

        Log.i(TAG, "Views added to main layout.");

        if (glView != null) {
            Log.i(TAG, "glView is NULL.");
            glView.onResume();
        }

        Looper.loop();
    }


    protected ARRenderer supplyRenderer() {
//        if (!checkCameraPermission()) {
//            Log.i(TAG, "No camera permission - restart the app");
//            return null;
//        }
        return new SimpleGLES20Renderer(this.ypr_xyz);
    }

//    private boolean checkCameraPermission() {
//        return ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
//    }
//
//    @Override
//    public void cameraPreviewStarted(int width, int height, int rate, int cameraIndex, boolean cameraIsFrontFacing) {
//        if (ARToolKit.getInstance().initialiseAR(width, height, "Data/camera_para.dat", cameraIndex, cameraIsFrontFacing)) { // Expects Data to be already in the cache dir. This can be done with the AssetUnpacker.
//            Log.i(TAG, "Camera initialised");
//        } else {
//            // Error
//            Log.e(TAG, "Error initialising camera. Cannot continue.");
//            mActivity.finish();
//        }
//
//        Toast.makeText(mContext, "Camera settings: " + width + "x" + height + "@" + rate + "fps", Toast.LENGTH_SHORT).show();
//
//        firstUpdate = true;
//    }

//    @Override
//    public void cameraPreviewFrame(byte[] frame) {
//        if (firstUpdate) {
//            // ARToolKit has been initialised. The renderer can now add markers, etc...
//            if (renderer.configureARScene()) {
//                Log.i(TAG, "Scene configured successfully VrOverlay");
//            } else {
//                // Error
//                Log.e(TAG, "Error configuring scene. Cannot continue.");
//                mActivity.finish();
//            }
//            firstUpdate = false;
//        }
//
////        Log.i(TAG, "cameraPreviewFrame VrOverlay");
//
//        if (ARToolKit.getInstance().convertAndDetect(frame)) {
//
//            Log.i(TAG, "Marker detected");
//
//            // Update the renderer as the frame has changed
//            if (glView != null) glView.requestRender();
//        }
//    }

    @Override
    public void cameraPreviewStarted(int width, int height, int rate, String pixelFormat, int cameraIndex, boolean cameraIsFrontFacing) {
        if (ARToolKit.getInstance().startWithPushedVideo(width, height, pixelFormat, "Data/camera_para.dat", cameraIndex, cameraIsFrontFacing)) {
            // Expects Data to be already in the cache dir. This can be done with the AssetUnpacker.
            Log.i(TAG, "cameraPreviewStarted(): Camera initialised");
        } else {
            // Error
            Log.e(TAG, "cameraPreviewStarted(): Error initialising camera. Cannot continue.");
            mActivity.finish();
        }

        Toast.makeText(mActivity, "Camera settings: " + width + "x" + height + "@" + rate + "fps", Toast.LENGTH_SHORT).show();
        firstUpdate = true;
    }

    @Override
    public void cameraPreviewFrame(byte[] frame, int frameSize) {

        if (firstUpdate) {
            // ARToolKit has been initialised. The renderer can now add markers, etc...
            if (renderer.configureARScene()) {
                Log.i(TAG, "cameraPreviewFrame(): Scene configured successfully");
            } else {
                // Error
                Log.e(TAG, "cameraPreviewFrame(): Error configuring scene. Cannot continue.");
                mActivity.finish();
            }
            firstUpdate = false;
        }

        if (ARToolKit.getInstance().convertAndDetect1(frame, frameSize)) {

            // Update the renderer as the frame has changed
            if (glView != null)
                glView.requestRender();
            onFrameProcessed();
        }
    }

    public void onFrameProcessed() {
    }

    @Override
    public void cameraPreviewStopped() {
        ARToolKit.getInstance().stopAndFinal();
    }
}
