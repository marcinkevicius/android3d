package com.example.kid.a3dstream;

import android.util.Log;
import android.widget.EditText;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by andrius on 05/10/2017.
 */

public class TcpClient implements Runnable{
    SensorFusionActivity mSensorFusionActivity;
    ReentrantLock lock = new ReentrantLock();
    public int framecount=0;

//    public native int parseStream(int stream_buffer_size, byte[] streambuffer);
//    public static final String SERVER_IP = "192.168.5.107"; //server IP address
    public static final int SERVER_PORT = 1234;
    // message to send to the server
    private String mServerMessage;
    public String SERVER_IP = "";
    private int[] ypr_xyz; // ypr_xyz - int values of Yaw, Pitch, Roll , X, Y, Z

    // while this is true, the server will continue running
    public boolean mRun = false;
    private BufferWrapper buffer_to_parse = new BufferWrapper();
    // used to send messages
    private PrintWriter mBufferOut;
    // used to read messages from the server

    private InputStream stream;
    private BufferWrapper video_stream_wraper;
    private int[] frame_sizes = new int[] {1000,1000,1000,1000,1000,1000,1000,1000,1000,1000};
    public ArrayList framesList = new ArrayList();

    public byte[] get_next_frame_from_list(){
        byte[] one_frame;
        lock.lock();
        try {
            one_frame = (byte[])framesList.remove(0);
        } finally {
            lock.unlock();
        }
        return one_frame;
    }

    public int indexOf(byte[] outerArray, byte[] smallerArray, int start_at) {
        for(int i = start_at; i < outerArray.length - smallerArray.length+1; ++i) {
            boolean found = true;
            for(int j = 0; j < smallerArray.length; ++j) {
                if (outerArray[i+j] != smallerArray[j]) {
                    found = false;
                    break;
                }
            }
            if (found) return i;
        }
        return -1;
    }


    public void mergeAndParseBuffer(BufferWrapper values){
        buffer_to_parse.addBuffer(values.streambuffer, values.bufferlen);

        int frame_count_index = 0;
        byte[] nalU = {0,0,0,1};
        int ndx = indexOf(buffer_to_parse.streambuffer, nalU, 0);
        if (ndx >= 0){
            if (ndx == 0){  // found nalu at 0 position, lets find next nalu to know frame end position
//                Log.e("NDK", "found nalu at start:"+ndx);
                ndx = indexOf(buffer_to_parse.streambuffer, nalU, 4);  // nalu at pos 0 so start from 4 pos
                frame_count_index = (frame_count_index+1)%frame_sizes.length;  // get new index of oldest frame
                frame_sizes[frame_count_index]=ndx;     // write frame size to oldest frame index array
//                Log.e("NDK", "frame ends at:"+ndx);
//                Log.e("NDK", "framebytes: "+
//                        (byte)buffer_to_parse.streambuffer[0]+(byte)buffer_to_parse.streambuffer[1]+
//                        (byte)buffer_to_parse.streambuffer[2]+(byte)buffer_to_parse.streambuffer[3]+
//                        (byte)buffer_to_parse.streambuffer[4]+(byte)buffer_to_parse.streambuffer[5]+
//                        (byte)buffer_to_parse.streambuffer[6]+(byte)buffer_to_parse.streambuffer[7]+
//                        (byte)buffer_to_parse.streambuffer[8]+(byte)buffer_to_parse.streambuffer[9]+
//                        (byte)buffer_to_parse.streambuffer[10]+(byte)buffer_to_parse.streambuffer[11]+
//                        (byte)buffer_to_parse.streambuffer[12]+(byte)buffer_to_parse.streambuffer[13]+
//                        (byte)buffer_to_parse.streambuffer[14]+(byte)buffer_to_parse.streambuffer[15]+
//                        (byte)buffer_to_parse.streambuffer[16]+(byte)buffer_to_parse.streambuffer[17]+
//                        (byte)buffer_to_parse.streambuffer[18]+(byte)buffer_to_parse.streambuffer[19]);
            } else {
//                Log.e("NDK", "start not found but nalu ends at:"+ndx);
            }
        } else {
            //Log.e("NDK", "nalu not found:"+ndx);
        }

        int parsedstreambytes;
        parsedstreambytes = ndx; //parseStream(buffer_to_parse.streambuffer.length, buffer_to_parse.streambuffer);

//        Log.e("JNI parser:", "parsed stream valid"+parsedstreambytes);

        if (parsedstreambytes>0){
            framecount++;

            // add frame from buffer to list by locking variable
            lock.lock();
            try { framesList.add(Arrays.copyOfRange(buffer_to_parse.streambuffer, 0, parsedstreambytes)); }
            finally { lock.unlock(); }
            // remove frame from buffer
            buffer_to_parse.streambuffer = Arrays.copyOfRange(buffer_to_parse.streambuffer, parsedstreambytes, buffer_to_parse.streambuffer.length);

        }
    }

    /**
     * Sends the message entered by client to the server
     *
     * @param message text entered by client
     */
    public void sendMessage(String message) {
        if (mBufferOut != null && !mBufferOut.checkError()) {
            mBufferOut.println(message);
//            Log.e("TCP Client", "sending message:"+message);
//            mBufferOut.flush();
        }
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (mBufferOut != null) {
            mBufferOut.flush();
            mBufferOut.close();
        }

        if (mSensorFusionActivity !=null)
            mSensorFusionActivity.cleanThread();

        stream = null;
        mBufferOut = null;
        mServerMessage = null;
    }

    public TcpClient(SensorFusionActivity sensor_fusion, int[]ypr_xyz) {
        this.mSensorFusionActivity=sensor_fusion;
        this.ypr_xyz = ypr_xyz;
    }

    @Override
    public void run() {

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
        mRun = true;
        int timeoutretrys=100;
        byte[] video_buffer = new byte[1024];
        byte[] video_buffer_5k = new byte[5120];
        byte[] video_buffer_10k = new byte[10240];
        int buffer_size_index = 0;
        int average_frame_size = 1000;
        int count_read = 0;
        video_stream_wraper = new BufferWrapper();

        int byte_count=0;
        float[] deviceOriantation;

        try {
            //here you must put your computer's IP address.
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

            Log.e("TcpClient", "C: Connecting...");

            //create a socket to make the connection with the server
            Socket socket = new Socket(serverAddr, SERVER_PORT);

            try {

                //sends the message to the server
                mBufferOut = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);

                //receives the message which the server sends back
//                mBufferIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                stream = socket.getInputStream();


                //in this while the client listens for the messages sent by the server
                while (mRun) {
                    if (buffer_size_index == 0)
                        count_read=stream.read(video_buffer);
                    if (buffer_size_index == 5)
                        count_read=stream.read(video_buffer_5k);
                    if (buffer_size_index == 10)
                        count_read=stream.read(video_buffer_10k);

                    if (count_read > 0 ) {

                        video_stream_wraper.bufferlen=count_read;
                        if (buffer_size_index == 0)
                            video_stream_wraper.streambuffer=video_buffer;
                        if (buffer_size_index == 5)
                            video_stream_wraper.streambuffer=video_buffer_5k;
                        if (buffer_size_index == 10)
                            video_stream_wraper.streambuffer=video_buffer_10k;

                        // count bytes received
                        byte_count+=count_read;
                        if (byte_count> 50000){ // send orientation every frame ~ each 50 000 bytes
                            byte_count=0;
                            if (mSensorFusionActivity!= null){
                                deviceOriantation = mSensorFusionActivity.getFusedOrientation();
                                sendMessage("fd:"+deviceOriantation[0]+"|"+deviceOriantation[1]+"|"+deviceOriantation[2]);
                            } else {sendMessage(":"+
                                    ypr_xyz[0]+" "+ypr_xyz[1]+" "+
                                    ypr_xyz[2]+" "+ypr_xyz[3]+" "+
                                    ypr_xyz[4]+" "+ypr_xyz[5]+";"
                            );
                            }

//                            Log.d("TcpClient", "fusion data: "+" "+deviceOriantation[0]+" "+deviceOriantation[1]+" "+deviceOriantation[2]);
                        }

                        timeoutretrys=100;
//                        Thread.sleep(1000);
                        if (count_read < 1024){
                            Log.d("TcpClient", "waiting 5ms, short data from network");
                            Thread.sleep(5);
                            buffer_size_index = 0;  // use small buffer
                        } else {
                            average_frame_size=0;
                            for (int i=0; i<frame_sizes.length; i++){       // lets calculate average frame size;
                                average_frame_size+=frame_sizes[i];
                            }
                            average_frame_size=average_frame_size/frame_sizes.length;
                            Log.d("TcpClient", "average_frame_size "+ average_frame_size);

                            if(average_frame_size < 50120 ) {       // adjust framebuffer for frame reading
                                buffer_size_index = 5;
                                Log.d("TcpClient", "using buffer 5k");
                            } else {
                                buffer_size_index = 10;
                                Log.d("TcpClient", "using buffer 10k");
                            }
                        }

                        mergeAndParseBuffer(video_stream_wraper);
                    } else {
                        Log.d("TcpClient", "no data ");
                        Thread.sleep(20);
                        timeoutretrys-=1;
                        Log.d("TcpClient", " timeoutretrys :" +timeoutretrys);
                        if (timeoutretrys==0){
                            stopClient();
                        }
                    }
                }

                Log.e("RESPONSE FROM SERVER", "S: Received Message: '" + mServerMessage + "'");

            } catch (Exception e) {

                Log.e("TcpClient", "S: Error", e);

            } finally {
                //the socket must be closed. It is not possible to reconnect to this socket
                // after it is closed, which means a new socket instance has to be created.
                socket.close();
            }

        } catch (Exception e) {
            Log.e("TCP", "C: Error", e);
        }
    }
}
