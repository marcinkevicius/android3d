package com.example.kid.a3dstream;

import android.app.Activity;
import android.media.*;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.TextureView;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayDeque;
import java.util.Locale;
import java.util.Queue;

/**
 * Simplifies the MediaCodec interface by wrapping around the buffer processing operations.
 */
public class MediaCodecWrapper implements Runnable{

    // Handler to use for {@code OutputSampleListener} and {code OutputFormatChangedListener}
    // callbacks
    private Handler mHandler; // ??????

    TcpClient tcpClient;
    Activity activity;
    private SurfaceView mPlaybackView;
    private boolean mLoging;

//    @Override
//    public void run() {
//        MediaCodec videoCodec = null;
//        final String mimeType = "video/avc";
//        MediaFormat decoderFormat = MediaFormat.createVideoFormat(mimeType, 720, 720);
//        try {
//            videoCodec = MediaCodec.createDecoderByType(mimeType);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        videoCodec.configure(decoderFormat, new Surface(mPlaybackView.getSurfaceTexture()), null,  0);
//
//        mDecoder = videoCodec;
//        mDecoder.start();
//
//        // lets start processing frames
//        while (tcpClient.framesList.size()<=0){
//            thread_sleep(10);
//        }
//
//        byte [] nextFrameToProcesss;
//        boolean result = false;
//
//        nextFrameToProcesss = (byte[])tcpClient.framesList.remove(0);
//
//        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
//        ByteBuffer[] inputBuffers = mDecoder.getInputBuffers();
//        mDecoder.getOutputBuffers();
//
//        while (true) {
//            if (tcpClient.framesList.size() > 0) {
//
////                ---------- new approuch START
//
//                // Get the next frame
////                byte[] frame = provider.nextFrame();
//
//                // Now we need to give it to the Codec to decode into the surface
//
//                // Get the input buffer from the decoder
//                int inputIndex = mDecoder.dequeueInputBuffer(0);// Pass in -1 here as in this example we don't have a playback time reference
//
//                // If  the buffer number is valid use the buffer with that index
//                if(inputIndex>=0) {
//                    Log.d("MCW", "write frame to buffer: "+tcpClient.framecount+" remaining frames:"+tcpClient.framesList.size());
//                    ByteBuffer buffer = inputBuffers[inputIndex];
//                    buffer.put(nextFrameToProcesss);
//                    // tell the decoder to process the frame
//
//                    mDecoder.queueInputBuffer(inputIndex, 0, nextFrameToProcesss.length, 0, 0);
//                    nextFrameToProcesss = tcpClient.get_next_frame_from_list();
//                }
//
////                MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
//                int outputIndex = mDecoder.dequeueOutputBuffer(info, 0);
//
//                switch (outputIndex) {
//                    case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
//                        Log.d("MCW", "INFO_OUTPUT_BUFFERS_CHANGED");
//                        mDecoder.getOutputBuffers();
//                        break;
//
//                    case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
//                        Log.d("MCW", "INFO_OUTPUT_FORMAT_CHANGED format : " + mDecoder.getOutputFormat());
//                        break;
//
//                    case MediaCodec.INFO_TRY_AGAIN_LATER:
//				        Log.d("MCW", "INFO_TRY_AGAIN_LATER");
//                        break;
//
//                    default:
//                        Log.d("MCW", "render frame to surface: "+tcpClient.framecount);
//                        mDecoder.releaseOutputBuffer(outputIndex, true);
//                        break;
//                }
////                if (outputIndex >= 0) {
////                    Log.d("MCW", "render frame to surface: "+tcpClient.framecount);
////                    mDecoder.releaseOutputBuffer(outputIndex, true);
////                }
//
//                // wait for the next frame to be ready, our server makes a frame every 250ms
//
////                ---------- new approuch END
//
//
//                Log.d("MCW", "done ");
//                thread_sleep(2);
//
//            } else {
//                if (!tcpClient.mRun && tcpClient.framesList.size()<=1 ){
//                    Log.d("MCW", "stop decoder ");
//                    thread_sleep(500);
//                    stopAndRelease();
//                    break;
//                }
//                thread_sleep(10);
//            }
//        }
//    }

    @Override
    public void run() {

        MediaCodec videoCodec = null;
        final String mimeType = "video/avc";
        MediaFormat decoderFormat = MediaFormat.createVideoFormat(mimeType, 720, 720);
        try {
            videoCodec = MediaCodec.createDecoderByType(mimeType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        decoderFormat.setInteger(MediaFormat.KEY_PRIORITY, 0);  // 0 - real time
       // decoderFormat.setInteger(MediaFormat.KEY_FRAME_RATE, 0);  // 0 - real time

        videoCodec.configure(decoderFormat, mPlaybackView.getHolder().getSurface() , null,  0);

        mDecoder = videoCodec;
        videoCodec.start();
        mInputBuffers = videoCodec.getInputBuffers();
        mOutputBuffers = videoCodec.getOutputBuffers();
        mOutputBufferInfo = new MediaCodec.BufferInfo[mOutputBuffers.length];
        mAvailableInputBuffers = new ArrayDeque<>(mOutputBuffers.length);
        mAvailableOutputBuffers = new ArrayDeque<>(mInputBuffers.length);
        if (mLoging)
            Log.d("MCW", "initialize MCW , Ibuffer: "+mInputBuffers.length + " Outputb : "+mOutputBuffers.length + "empty"+mAvailableInputBuffers.isEmpty());

        // lets start processing frames
        while (tcpClient.framesList.size()<=0){
            thread_sleep(10);
            Log.d("MCW", "no Frames to proccess, sleeping 10ms ");
        }

        byte [] nextFrameToProcesss;
        boolean result = false;

        nextFrameToProcesss = (byte[])tcpClient.framesList.remove(0);

        while (true) {
            if (tcpClient.framesList.size() > 0) {

                // writeSample
                // Try to submit the sample frame to the codec and if successful advance the to next frame
                try {
                    if (mLoging)
                        Log.d("MCW", "framesList size : " + tcpClient.framesList.size() + "frame:" +tcpClient.framecount);
                    result = writeSample(ByteBuffer.wrap(nextFrameToProcesss), null, tcpClient.framecount, 0);
                    if (mLoging)
                        Log.d("MCW", "result : " + result);
                } catch (MediaCodecWrapper.WriteException e) {
                    Log.d("MCW", "result : wireSample error");
                    e.printStackTrace();
                }

                if (result) { // if succesfully writen frame, proceed to next frame
                    nextFrameToProcesss = tcpClient.get_next_frame_from_list();
                }


                MediaCodec.BufferInfo out_bufferInfo = new MediaCodec.BufferInfo();
                if (mLoging){
                    Log.d("MCW", "peeksample :"+ peekSample(out_bufferInfo));

                    Log.d("MCW", "bufferinfo size " + out_bufferInfo.size);

                    // Pop the sample off the queue and send it to {@link Surface}
                    Log.d("MCW", "popsample "+ popSample(true));

                    Log.d("MCW", "done ");
                } else {
                    peekSample(out_bufferInfo);
                    popSample(true);
                }
                thread_sleep(10);
            } else {
                if (!tcpClient.mRun && tcpClient.framesList.size()<=1 ){
                    if (mLoging)
                        Log.d("MCW", "stop decoder ");
                    thread_sleep(500);
                    stopAndRelease();
                    break;
                } else {
                    Log.d("MCW", "no Frames to proccess, sleeping 10ms ");
                }
                thread_sleep(10);
            }
        }
    }


    // Callback when media output format changes.
    public interface OutputFormatChangedListener {
        void outputFormatChanged(MediaCodecWrapper sender, MediaFormat newFormat);
    }

    private OutputFormatChangedListener mOutputFormatChangedListener = null;

    /**
     * Callback for decodes frames. Observers can register a listener for optional stream
     * of decoded data
     */
    public interface OutputSampleListener {
        void outputSample(MediaCodecWrapper sender, MediaCodec.BufferInfo info, ByteBuffer buffer);
    }

    /**
     * The {@link MediaCodec} that is managed by this class.
     */
    private MediaCodec mDecoder;

    // References to the internal buffers managed by the codec. The codec
    // refers to these buffers by index, never by reference so it's up to us
    // to keep track of which buffer is which.
    private ByteBuffer[] mInputBuffers;
    private ByteBuffer[] mOutputBuffers;

    // Indices of the input buffers that are currently available for writing. We'll
    // consume these in the order they were dequeued from the codec.
    private Queue<Integer> mAvailableInputBuffers;

    // Indices of the output buffers that currently hold valid data, in the order
    // they were produced by the codec.
    private Queue<Integer> mAvailableOutputBuffers;

    // Information about each output buffer, by index. Each entry in this array
    // is valid if and only if its index is currently contained in mAvailableOutputBuffers.
    private MediaCodec.BufferInfo[] mOutputBufferInfo;

    //public MediaCodecWrapper(MediaCodec codec, TcpClient tcpClient) {
    public MediaCodecWrapper(SurfaceView mPlaybackView, TcpClient tcpClient, boolean loging, Activity activity) {
        this.activity = activity;
        this.tcpClient=tcpClient;
        this.mPlaybackView=mPlaybackView;
        this.mLoging=loging;
        if (mLoging)
            Log.d("MCW", "created class mediacodecwraper: ");
    }

    /**
     * Releases resources and ends the encoding/decoding session.
     */
    public void stopAndRelease() {
        mDecoder.stop();
        mDecoder.release();
        mDecoder = null;
        //mHandler = null;
    }

    /**
     * Getter for the registered {@link OutputFormatChangedListener}
     */
    public OutputFormatChangedListener getOutputFormatChangedListener() {
        return mOutputFormatChangedListener;
    }

    /**
     *
     * @param outputFormatChangedListener the listener for callback.
     * @param handler message handler for posting the callback.
     */
    public void setOutputFormatChangedListener(final OutputFormatChangedListener
                                                       outputFormatChangedListener, Handler handler) {
        mOutputFormatChangedListener = outputFormatChangedListener;

        // Making sure we don't block ourselves due to a bad implementation of the callback by
        // using a handler provided by client.
        mHandler = handler;
        if (outputFormatChangedListener != null && mHandler == null) {
            if (Looper.myLooper() != null) {
                mHandler = new Handler();
            } else {
                throw new IllegalArgumentException(
                        "Looper doesn't exist in the calling thread");
            }
        }
    }

    /**
     * Write a media sample to the decoder.
     *
     * A "sample" here refers to a single atomic access unit in the media stream. The definition
     * of "access unit" is dependent on the type of encoding used, but it typically refers to
     * a single frame of video or a few seconds of audio. {@link android.media.MediaExtractor}
     * extracts data from a stream one sample at a time.
     *
     * @param input A ByteBuffer containing the input data for one sample. The buffer must be set
     * up for reading, with its position set to the beginning of the sample data and its limit
     * set to the end of the sample data.
     *
     * @param presentationTimeUs  The time, relative to the beginning of the media stream,
     * at which this buffer should be rendered.
     *
     * @param flags Flags to pass to the decoder. See {@link MediaCodec#queueInputBuffer(int,
     * int, int, long, int)}
     *
     * @throws MediaCodec.CryptoException
     */
    public boolean writeSample(final ByteBuffer input,
                               final MediaCodec.CryptoInfo crypto,
                               final long presentationTimeUs,
                               final int flags) throws MediaCodec.CryptoException, WriteException {
        boolean result = false;
        int size = input.remaining();

        // check if we have dequed input buffers available from the codec
        if (!mAvailableInputBuffers.isEmpty()) {
            int index = mAvailableInputBuffers.remove();
            ByteBuffer buffer = mInputBuffers[index];

            // we can't write our sample to a lesser capacity input buffer.
            if (size > buffer.capacity()) {
                throw new MediaCodecWrapper.WriteException(String.format(Locale.US,
                        "Insufficient capacity in MediaCodec buffer: "
                                + "tried to write %d, buffer capacity is %d.",
                        input.remaining(),
                        buffer.capacity()));
            }

            buffer.clear();
            buffer.put(input);

            // Submit the buffer to the codec for decoding. The presentationTimeUs
            // indicates the position (play time) for the current sample.
            mDecoder.queueInputBuffer(index, 0, size, presentationTimeUs, flags);

            result = true;
        }
        return result;
    }

    private static MediaCodec.CryptoInfo sCryptoInfo = new MediaCodec.CryptoInfo();

    /**
     * Write a media sample to the decoder.
     *
     * A "sample" here refers to a single atomic access unit in the media stream. The definition
     * of "access unit" is dependent on the type of encoding used, but it typically refers to
     * a single frame of video or a few seconds of audio. {@link android.media.MediaExtractor}
     * extracts data from a stream one sample at a time.
     *
     * @param extractor  Instance of {@link android.media.MediaExtractor} wrapping the media.
     *
     * @param presentationTimeUs The time, relative to the beginning of the media stream,
     * at which this buffer should be rendered.
     *
     * @param flags  Flags to pass to the decoder. See {@link MediaCodec#queueInputBuffer(int,
     * int, int, long, int)}
     *
     * @throws MediaCodec.CryptoException
     */
    public boolean writeSample(final MediaExtractor extractor,
                               final boolean isSecure,
                               final long presentationTimeUs,
                               int flags) {
        boolean result = false;

        if (!mAvailableInputBuffers.isEmpty()) {
            int index = mAvailableInputBuffers.remove();
            ByteBuffer buffer = mInputBuffers[index];

            // reads the sample from the file using extractor into the buffer
            int size = extractor.readSampleData(buffer, 0);
            if (size <= 0) {
                flags |= MediaCodec.BUFFER_FLAG_END_OF_STREAM;
            }

            // Submit the buffer to the codec for decoding. The presentationTimeUs
            // indicates the position (play time) for the current sample.
            if (!isSecure) {
                mDecoder.queueInputBuffer(index, 0, size, presentationTimeUs, flags);
            } else {
                extractor.getSampleCryptoInfo(sCryptoInfo);
                mDecoder.queueSecureInputBuffer(index, 0, sCryptoInfo, presentationTimeUs, flags);
            }

            result = true;
        }
        return result;
    }

    /**
     * Performs a peek() operation in the queue to extract media info for the buffer ready to be
     * released i.e. the head element of the queue.
     *
     * @param out_bufferInfo An output var to hold the buffer info.
     *
     * @return True, if the peek was successful.
     */
    public boolean peekSample(MediaCodec.BufferInfo out_bufferInfo) {
        // dequeue available buffers and synchronize our data structures with the codec.
        update();
        boolean result = false;
        if (!mAvailableOutputBuffers.isEmpty()) {
            int index = mAvailableOutputBuffers.peek();
            MediaCodec.BufferInfo info = mOutputBufferInfo[index];
            // metadata of the sample
            out_bufferInfo.set(info.offset, info.size, info.presentationTimeUs, info.flags);
            result = true;
        }
        return result;
    }
    /**
     * Processes, releases and optionally renders the output buffer available at the head of the
     * queue. All observers are notified with a callback. See {@link
     * OutputSampleListener#outputSample(MediaCodecWrapper, android.media.MediaCodec.BufferInfo,
     * java.nio.ByteBuffer)}
     *
     * @param render True, if the buffer is to be rendered on the {@link Surface} configured
     *
     */
    public boolean popSample(boolean render) {
        // dequeue available buffers and synchronize our data structures with the codec.
        update();
        boolean result=false;
        if (!mAvailableOutputBuffers.isEmpty()) {
            int index = mAvailableOutputBuffers.remove();

            // releases the buffer back to the codec
            mDecoder.releaseOutputBuffer(index, render);
            result=true;
        }
        return result;
    }

    /**
     * Synchronize this object's state with the internal state of the wrapped
     * MediaCodec.
     */
    private void update() {
        // BEGIN_INCLUDE(update_codec_state)
        int index;

        if (mLoging)
            Log.d("MCW", "mAvailableInputBuffers:"+mAvailableInputBuffers.size()+", mAvailableOutputBuffers:"+mAvailableOutputBuffers.size());

        // Get valid input buffers from the codec to fill later in the same order they were
        // made available by the codec.
        while ((index = mDecoder.dequeueInputBuffer(0)) != MediaCodec.INFO_TRY_AGAIN_LATER) {
            mAvailableInputBuffers.add(index);
        }


        // Likewise with output buffers. If the output buffers have changed, start using the
        // new set of output buffers. If the output format has changed, notify listeners.
        MediaCodec.BufferInfo info = new MediaCodec.BufferInfo();
        while ((index = mDecoder.dequeueOutputBuffer(info, 0)) !=  MediaCodec.INFO_TRY_AGAIN_LATER) {
            switch (index) {
                case MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED:
                    mOutputBuffers = mDecoder.getOutputBuffers();
                    mOutputBufferInfo = new MediaCodec.BufferInfo[mOutputBuffers.length];
                    mAvailableOutputBuffers.clear();
                    break;
                case MediaCodec.INFO_OUTPUT_FORMAT_CHANGED:
                    Log.d("MCW", "Video format changed:"+mDecoder.getOutputFormat().getInteger(MediaFormat.KEY_HEIGHT));

                    activity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mPlaybackView.getHolder().setFixedSize(
                                    mDecoder.getOutputFormat().getInteger(MediaFormat.KEY_WIDTH),
                                    mDecoder.getOutputFormat().getInteger(MediaFormat.KEY_HEIGHT)
                            );

                        }
                    });

                    if (mOutputFormatChangedListener != null) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mOutputFormatChangedListener
                                        .outputFormatChanged(MediaCodecWrapper.this,
                                                mDecoder.getOutputFormat());

                            }
                        });
                    }
                    break;
                default:
                    // Making sure the index is valid before adding to output buffers. We've already
                    // handled INFO_TRY_AGAIN_LATER, INFO_OUTPUT_FORMAT_CHANGED &
                    // INFO_OUTPUT_BUFFERS_CHANGED i.e all the other possible return codes but
                    // asserting index value anyways for future-proofing the code.
                    if (index >= 0) {
                        mOutputBufferInfo[index] = info;
                        mAvailableOutputBuffers.add(index);
                    } else {
                        throw new IllegalStateException("Unknown status from dequeueOutputBuffer");
                    }
                    break;
            }

        }
        // END_INCLUDE(update_codec_state)

    }

//    private class WriteException extends Throwable {
    public class WriteException extends Throwable {
        private WriteException(final String detailMessage) {
            super(detailMessage);
        }
    }

    private void thread_sleep(int time_ms){
        try {
            Thread.sleep(time_ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
