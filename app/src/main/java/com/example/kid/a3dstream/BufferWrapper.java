package com.example.kid.a3dstream;

/**
 * Created by andrius on 16/10/2017.
 */

public class BufferWrapper {
    public byte[] streambuffer;
    public int bufferlen;

    public BufferWrapper(){
        bufferlen=0;
        streambuffer = new byte[0];
    }
    public void addBuffer(byte[] buffet_to_add, int add_leng){

        byte[] new_buffer = new byte[streambuffer.length + add_leng];

        // add current buffer to new
        System.arraycopy(streambuffer, 0, new_buffer, 0, streambuffer.length);
        // add additional buffer to new
        System.arraycopy(buffet_to_add, 0, new_buffer, streambuffer.length, add_leng);

        // our new data update
        streambuffer = new_buffer;
    }
}
