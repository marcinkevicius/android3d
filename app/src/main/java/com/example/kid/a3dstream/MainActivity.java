package com.example.kid.a3dstream;

import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TcpClient mTcpClient;
    private EditText server_ip_addr;
    private SurfaceView mPlaybackView;
    private LinearLayout mLinearNavigation;
    private MediaCodecWrapper mCodecWrapper;
    private SensorFusionActivity mSensorFusionActivity;
    private VrOverlay mVrOverlay;
    private int Wwidth, Wheight; // android display WxH in pixels
    private Activity this_activity;

    // dummy frame callback for future uses , MAYBE
    public void gotFrameCallback(byte[] buf, int size, int frameNum, boolean isKeyFrame, int width, int height) {}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.screenBrightness = 1.0f;
        getWindow().setAttributes(params);

        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // Start get android display WxH in pixels
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        Wwidth = size.x;
        Wheight = size.y;
        this_activity = this;

        float size_koef=(float) 1920/size.x;
//        Log.e("WidthHeightkof", "" + size_koef);
//        Log.e("Width", "" + Wwidth);
//        Log.e("height", "" + Wheight);
//        Log.e("NewWidth", "" + ((int)(1920/size_koef)));
//        Log.e("Newheight", "" + ((int)(960/size_koef)));

        // End get android display WxH in pixels

        server_ip_addr = (EditText) findViewById(R.id.ip_addr);
        mLinearNavigation = (LinearLayout) findViewById(R.id.linearNavigation);

        mPlaybackView = (SurfaceView) findViewById(R.id.PlaybackView);
        mPlaybackView.getHolder().setFixedSize(Wwidth, (int)(960/size_koef));
        mPlaybackView.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN);
                mLinearNavigation.setVisibility(View.VISIBLE);
                mLinearNavigation.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLinearNavigation.setVisibility(View.GONE);
                    }
                }, 3000);
                return false;
            }
        });

        mVrOverlay = new VrOverlay(this, this, (TextureView) findViewById(R.id.texture));
        new Thread(mVrOverlay).start();

//        mSensorFusionActivity = new SensorFusionActivity(this);
//        new Thread(mSensorFusionActivity).start();

        // Example of a call to a native method
        final Button protocol_button = (Button) findViewById(R.id.btn_protocol);
        protocol_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView tvInfo = (TextView) findViewById(R.id.tv_info);
//                tvInfo.setText(urlprotocolinfo());
            }
        });

        final Button format_button = (Button) findViewById(R.id.btn_format);
        format_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView tvInfo = (TextView) findViewById(R.id.tv_info);
//                tvInfo.setText(avformatinfo());
            }
        });

        final Button codec_button = (Button) findViewById(R.id.btn_codec);
        codec_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView tvInfo = (TextView) findViewById(R.id.tv_info);
//                tvInfo.setText(avcodecinfo());
            }
        });

        final Button filter_button = (Button) findViewById(R.id.btn_filter);
        filter_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                TextView tvInfo = (TextView) findViewById(R.id.tv_info);
                //tvInfo.setText(avfilterinfo());

                mSensorFusionActivity.cleanThread();
            }
        });

        final Button connect_button = (Button) findViewById(R.id.tcp_connect);
        connect_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView tvInfo = (TextView) findViewById(R.id.tv_info);
                String[] parts = server_ip_addr.getText().toString().split("\\.");
                int x,y;
                try {
                    x = Integer.parseInt(parts[0]);
                    y = Integer.parseInt(parts[1]);
                } catch(NumberFormatException e) {
                    return;
                } catch(NullPointerException e) {
                    return;
                }

                if (x<=254 && x >=0 && y<=254 && y >=0 ){
                    tvInfo.setText("connecting to server");
                } else {
                    tvInfo.setText("bad ip address server");
                    return;
                }


                mTcpClient = new TcpClient(mSensorFusionActivity, mVrOverlay.ypr_xyz); // pass referances to already defined variables
                mTcpClient.SERVER_IP="192.168."+x+"."+y;
                mCodecWrapper = new MediaCodecWrapper(mPlaybackView, mTcpClient, false, this_activity);

                // start raw stream reading and parsing
                new Thread(mTcpClient).start();

                // start parsed stream decoding
                new Thread(mCodecWrapper).start();
                Log.d("test", "this should come before tcpclient ");
                mLinearNavigation = (LinearLayout) findViewById(R.id.linearNavigation);
                mLinearNavigation.setVisibility(View.GONE);
            }
        });

        TextView tvInfo = (TextView) findViewById(R.id.tv_info);
//        tvInfo.setText(String.valueOf(stringFromJNI()));
//        tvInfo.setText(String.valueOf(stringFromJNI()));

    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */

//    public native String urlprotocolinfo();
//    public native String avformatinfo();
//    public native String avcodecinfo();
//    public native String avfilterinfo();

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
//    public native int stringFromJNI();
//    public native int parseStream(int stream_buffer_size, byte[] streambuffer);

}